/*! \file
 * \brief Implements the GNU Radio part of SatNOGS-Radio */
#include "hamlib/rig.h"
#include "satnogs_gr.hpp"
#include <stdlib.h>

using namespace gr;
namespace dll = boost::dll;


/** \brief Constructs new SatNOGS-Radio - scans library path for Plugins. */
satnogs_radio::satnogs_radio()
{
	// TODO: use maybe a config file?
	char *satnogs_radio_lib_env = getenv("SATNOGS_RADIO_LIB");
	if (!satnogs_radio_lib_env) {
		std::cerr << "Error: SatNOGS-Radio library path must be specified as "
		          << "environment variable SATNOGS_RADIO_LIB" << std::endl;
		exit(1);
	}
	std::string path = satnogs_radio_lib_env;
	boost::filesystem::path lib_path(path);
	boost::shared_ptr<satnogs_plugin_block> plugin;

	for (auto& entry : boost::filesystem::directory_iterator(lib_path)) {
		if (entry.path().extension() == ".so" || entry.path().extension() == ".dll") {
			rig_debug(RIG_DEBUG_VERBOSE, "Loading Plugin\n");
			plugin = dll::import<satnogs_plugin_block>(entry, "plugin");
			switch (plugin->get_plugin_type()) {
			case satnogs_plugin_type::SATNOGS_DECODER:
				rig_debug(RIG_DEBUG_VERBOSE, "DECODER Type\n");
				this->decoder_registry[plugin->name()] =
				    boost::dynamic_pointer_cast<satnogs_decoder_block,
				                                satnogs_plugin_block>(plugin);
				break;
			case satnogs_plugin_type::SATNOGS_DEMODULATOR:
				rig_debug(RIG_DEBUG_VERBOSE, "DEMODULATOR Type\n");
				this->demodulator_registry[plugin->name()] =
				    boost::dynamic_pointer_cast<satnogs_demodulator_block,
				                                satnogs_plugin_block>(plugin);
				break;
			}
		}
	}

	this->tb = gr::make_top_block("top_block");
}

satnogs_radio::~satnogs_radio() {}


/** \brief Checks if all parameters are valid and if so starts radio. 
 * If the radio is already running it does nothing */
void satnogs_radio::start_radio()
{
	if (this->running)
		return;

	if (!(this->tb))
		this->tb = gr::make_top_block("top_block");

	// only allowed to run if source and mode is selected
	if (!(this->valid_mode_string() && this->valid_source_string()))
		return;

	// if source is not SDR (a file!) than we need a filename
	if (this->source_string != "SDR" && !this->valid_source_filename_string())
		return;

	if (!this->instantiate_source())
		return;

	if (!this->instantiate_demodulator())
		return;

	this->tb->start();
	this->running = true;
	if (this->exit_after_finish)
		this->stop_and_exit();
}

/** \brief Set device argument string and either restarts the radio or tires to start it */
void satnogs_radio::set_arg(const std::string& args)
{
	if (this->args == args)
		return;

	this->args = args;
	if (this->running) {
		this->stop_radio();
		this->start_radio();
	} else {
		this->start_radio();
	}
}

/** \brief returns current audio samplerate */
int satnogs_radio::get_samp_rate() const { return this->audio_samp_rate; }

/** \brief sets autdio samplerate and restarts the radio or tries to start it*/
void satnogs_radio::set_samp_rate(int audio_samp_rate)
{
	this->audio_samp_rate = audio_samp_rate;

	if (this->running) {
		this->stop_radio();
		this->start_radio();
	} else {
		this->start_radio();
	}
}

/** \brief sets the source string and tries to start the radio */
void satnogs_radio::set_source(const std::string& source)
{
	if (this->source_string == source)
		return;

	rig_debug(RIG_DEBUG_VERBOSE, "set source\n");
	this->source_string = source;

	if (this->running)
		this->instantiate_source();
	else
		this->start_radio();
}

/** \brief instantiates and connects sources based on soruce string */
bool satnogs_radio::instantiate_source()
{
	this->tb->lock();
	if (this->source) {
		this->tb->disconnect(this->source);
		this->source.reset();
	}

	if (source_string.compare("SDR") == 0) {
		rig_debug(RIG_DEBUG_VERBOSE, "SDR Selected\n");
		if (!this->sdr) {
			this->sdr = osmosdr::source::make(this->args);
			this->sdr->set_sample_rate(this->rx_sampling_rate);
			this->sdr->set_center_freq(this->f_rx - this->lo_offset);
		}
		this->source = sdr;
		this->update_float_source_state(false);
	} else if (source_string.compare("IQFile") == 0 && valid_source_filename_string()) {
		rig_debug(RIG_DEBUG_VERBOSE, "IQFile Selected\n");
		if (this->iqfile_source)
			this->iqfile_source.reset();

		this->iqfile_source = gr::blocks::file_source::make(
		    sizeof(gr_complex), this->source_filename.c_str());
		this->update_float_source_state(false);
		this->source = this->iqfile_source;
	} else if (source_string.compare("WavFile") == 0 && valid_source_filename_string()) {
		rig_debug(RIG_DEBUG_VERBOSE, "WavFile Selected\n");
		if (this->wavfile_source)
			this->wavfile_source.reset();

		this->wavfile_source =
		    gr::blocks::wavfile_source::make(this->source_filename.c_str());
		this->source = wavfile_source;
		this->update_float_source_state(true);
	} else if (source_string.compare("OggFile") == 0 && valid_source_filename_string()) {
		rig_debug(RIG_DEBUG_VERBOSE, "WavFile Selected\n");
		this->update_float_source_state(true);
	}

	if (this->demodulator_block && this->source) {
		this->tb->disconnect(this->source);
		this->tb->connect(this->source, 0, this->demodulator_block, 0);
	}

	this->tb->unlock();
	return true;
}

/** \brief checks if the source changed format between complex and float.
 * reinstantiates demodulator if neccesary. */
void satnogs_radio::update_float_source_state(bool new_state)
{
	if (this->is_float_source == new_state)
		return;

	this->is_float_source = new_state;

	if (this->running)
		this->instantiate_demodulator();
}

/** \brief sets modulator string and reinstantiates modulator if running
 * or tries to start the radio otherwise.
 */
void satnogs_radio::set_mode(std::string mode)
{
	if (this->mode_string == mode)
		return;

	this->mode_string = mode;

	if (this->running)
		this->instantiate_demodulator();
	else
		this->start_radio();
}

/** \brief Instantiates and connects demodulator. */
bool satnogs_radio::instantiate_demodulator()
{
	if (!this->valid_mode_string())
		return false;

	rig_debug(RIG_DEBUG_VERBOSE, "instantiate demodulator\n");
	this->tb->lock();
	if (this->demodulator_block) {
		this->tb->disconnect(this->demodulator_block);
		this->demodulator_block.reset();
	}

	const char* mode_name = this->demodulator_registry[this->mode_string]->name().c_str();
	rig_debug(RIG_DEBUG_VERBOSE, "LOAD demodulator Block from plugin: %s\n", mode_name);

	this->demodulator = this->demodulator_registry[this->mode_string];
	this->demodulator_block = this->demodulator->block(this->audio_samp_rate,
	                                                   this->rx_sampling_rate,
	                                                   this->lo_offset,
	                                                   this->is_float_source);
	this->connect_demodulator_outputs();

	if (this->source && this->demodulator_block) {
		this->tb->connect(this->source, 0, this->demodulator_block, 0);
	}

	this->instantiate_decoder();
	this->instantiate_ogg_output();

	this->tb->unlock();

	return true;
}

/** \brief Connects demodulator outputs and instantiates waterfall */
void satnogs_radio::connect_demodulator_outputs()
{
	if (!this->demodulator || !this->demodulator_block) 
		return;

	this->connect_audio_out();

	if (this->demodulator->has_binary_out()) {
		if (!this->udp_sink)
			this->udp_sink = blocks::udp_sink::make(sizeof(int8_t), "127.0.0.1", 1234);

		this->tb->connect(this->demodulator_block, 1, this->udp_sink, 0);
	}

	this->instantiate_waterfall();
}

void satnogs_radio::connect_audio_out() {
	if (this->demodulator->has_audio_out() && this->use_audio_out) {
		if (!this->audio_out)
			this->audio_out = audio::sink::make(this->audio_samp_rate);

		if (this->null_float_sink) {
			this->tb->disconnect(this->null_float_sink);
			this->null_float_sink.reset();
		}

		this->tb->connect(this->demodulator_block, 0, this->audio_out, 0);
		this->tb->connect(this->demodulator_block, 0, this->audio_out, 1);
	}

	if (this->demodulator->has_audio_out() && (!this->use_audio_out)) {
		if (!this->null_float_sink)
			this->null_float_sink = gr::blocks::null_sink::make(sizeof(float));
		this->tb->connect(this->demodulator_block, 0, this->null_float_sink, 0);
	}
}

/** \brief Instantiates waterfalls and connect it to the demodulator */
void satnogs_radio::instantiate_waterfall() {
	if (this->waterfall_sink)
		this->waterfall_sink.reset();

	if (this->demodulator->has_complex_out() && !(this->waterfall_file_path.empty()) &&
	    !(this->is_float_source)) {

		this->waterfall_sink = gr::satnogs::waterfall_sink::make(this->audio_samp_rate, 0.0, 10, 1024, this->waterfall_file_path, 1);

		this->tb->connect(this->demodulator_block, 2, this->waterfall_sink, 0);
	}
}

/** \brief Sets deocder string and reinstantiates decoder if running */
void satnogs_radio::set_decoder(std::string decoder)
{
	if (this->decoder_string == decoder)
		return;

	this->decoder_string = decoder;

	if (this->running)
		this->instantiate_decoder();
	else
		this->start_radio();
}

bool satnogs_radio::instantiate_decoder()
{
	if (!valid_decoder_string())
		return false;

	const char* decoder_name = this->decoder_registry[decoder_string]->name().c_str();
	rig_debug(RIG_DEBUG_VERBOSE, "LOAD decoder Block from plugin: %s\n", decoder_name);


	if (!this->demodulator_block) {
		std::cerr
		    << "Error: no demodulator block - no decoder possible without a demodulator"
		    << std::endl;
		return false;
	}

	if (!this->demodulator->has_binary_out()) {
		std::cerr << "Error: this demodulator has no binary out so it is not possible to "
		             "use a decoder"
		          << std::endl;
		return false;
	}

	this->tb->lock();
	if (this->decoder_block) {
		// this->tb->disconnect(this->decoder_block);
		this->decoder_block.reset();
	}



	this->decoder = this->decoder_registry[decoder_string];
	this->decoder_block = this->decoder->block();
	this->tb->connect(this->demodulator_block, 1, this->decoder_block, 0);

	if (!this->frame_file_prefix.empty()) {
		if (this->frame_file_sink)
			this->frame_file_sink.reset();

		this->frame_file_sink = gr::satnogs::frame_file_sink::make(this->frame_file_prefix, 0);
		this->tb->msg_connect(this->decoder_block, "out", this->frame_file_sink, "frame");
	}

	this->tb->unlock();

	return true;
}

/** \brief Sets center frequency and if running in SDR-Mode sets SDR-Frequency */
void satnogs_radio::set_center_freq(double freq)
{
	this->f_rx = freq;
	if (this->running && this->source == this->sdr)
		this->sdr->set_center_freq(this->f_rx - this->lo_offset);
}

/** \brief Sets LO-Offset and updates demodulator Block and SDR if running*/
void satnogs_radio::set_lo(double lo)
{
	this->lo_offset = lo;
	if (this->demodulator_block)
		this->demodulator->set_center_freq(this->lo_offset);
	if (this->running && this->source == this->sdr)
		this->sdr->set_center_freq(this->f_rx - this->lo_offset);
}

/** \brief Sets receiving sample rate and restarts radio if running */
void satnogs_radio::set_rx_rate(double rx_rate)
{
	this->rx_sampling_rate = rx_rate;
	if (this->running) {
		this->stop_radio();
		this->start_radio();
	}
}

/** \brief set osmo-sdr main gain */
void satnogs_radio::set_gain(double gain)
{
	if (this->source == this->sdr)
		this->sdr->set_gain(gain);
}

/** \brief set osmo-sdr if main gain */
void satnogs_radio::set_if_gain(double gain)
{
	if (this->source == this->sdr)
		this->sdr->set_if_gain(gain);
}

/** \brief set osmo-sdr bb main gain */
void satnogs_radio::set_bb_gain(double gain)
{
	if (this->source == this->sdr)
		this->sdr->set_bb_gain(gain);
}

/** \brief Stops GNU Radio if running and resets all blocks.
 * After this there shoudn't be any more CPU consumption.*/
void satnogs_radio::stop_radio()
{
	if (this->running) {
		this->tb->stop();
		this->running = false;
		this->tb->wait();

		// TODO: Check wether more bocks need to be reset
		if (this->source)
			this->source.reset();

		if (this->sdr)
			this->sdr.reset();

		if (this->demodulator_block)
			this->demodulator_block.reset();

		if (this->audio_out)
			this->audio_out.reset();

		if (this->decoder_block)
			this->decoder_block.reset();

		this->tb.reset();
	}
}


/** \brief sets if radio should exit after finishing */
void satnogs_radio::set_exit(int i)
{
	if (i)
		this->exit_after_finish = true;
	else
		this->exit_after_finish = false;

	if (this->running && this->exit_after_finish)
		this->stop_and_exit();
}


/** \brief sets if audio out should be played back by the soundcard */
void satnogs_radio::set_audio_out(int i)
{
	bool before = this->use_audio_out;

	if (i)
		this->use_audio_out = true;
	else
		this->use_audio_out = false;

	if (this->running && before != this->use_audio_out) {
		this->tb->lock();
		if (this->audio_out) {
			this->tb->disconnect(audio_out);
			this->audio_out.reset();
		}
		this->connect_audio_out();
		this->tb->unlock();
	}
}

/** \brief wait until the flowgraph finished and exit application */
void satnogs_radio::stop_and_exit()
{
	// if the source is a SDR the graph will never finish!
	if (this->source == this->sdr)
		return;

	this->tb->wait();
	this->tb->stop();
	exit(0);
}

/** \brief Sets ogg output filename and reinstantiates ogg output */
void satnogs_radio::set_ogg_filename(const std::string &filename)
{
	if (this->ogg_output_file == filename)
		return;
	if (filename.empty())
		return;

	this->ogg_output_file = filename;

	if (this->running) {
		this->tb->lock();
		this->instantiate_ogg_output();
		this->tb->unlock();
	}
}

/** \brief instantaites the ogg output file */
void satnogs_radio::instantiate_ogg_output() {
	if (this->ogg_output_file.empty())
		return;

	if (this->ogg_encoder) {
		this->ogg_encoder.reset();
	}

	// Quality is set from 1.0 (best) to 0.1 (worst)
	if (this->demodulator_block && this->demodulator->has_audio_out()) {
		this->ogg_encoder = satnogs::ogg_encoder::make((char *) this->ogg_output_file.c_str(), this->audio_samp_rate, 1.0);
		this->tb->connect(this->demodulator_block, 0, this->ogg_encoder, 0);
	}
}

/** \brief sets frame file prefix and reintantiates the frame file sink if radio is running */
void satnogs_radio::set_frame_file_prefix(const std::string &prefix) {
	if (this->frame_file_prefix == prefix)
		return;

	if (prefix.empty())
		return;

	this->frame_file_prefix = prefix;
	if (this->running && this->decoder_block) {
		this->tb->lock();
		if (this->frame_file_sink)
			this->frame_file_sink.reset();
		this->frame_file_sink = gr::satnogs::frame_file_sink::make(this->frame_file_prefix, 0);
		this->tb->msg_connect(this->decoder_block, "out", this->frame_file_sink, "frame");
		this->tb->unlock();
	}
}

/** \brief sets waterfall filename and reintantiates the waterfall sink if radio is running */
void satnogs_radio::set_waterfall_file(const std::string &waterfall) {
	if (this->waterfall_file_path == waterfall)
		return;

	if (waterfall.empty())
		return;

	this->waterfall_file_path = waterfall;
	if (this->running && this->demodulator_block) {
		this->tb->lock();
		this->instantiate_waterfall();
		this->tb->unlock();
	}
}

/** \brief Set source filename string and restart radio or try to start radio */
void satnogs_radio::set_source_filename(char* filename)
{
	this->source_filename = filename;
	if (this->running && this->source_string != "SDR") {
		this->stop_radio();
		this->start_radio();
	}
	if (!this->running)
		this->start_radio();
}

/** \brief checks if mode string is valid - that means not empty and the mode is avaiblable via plugin */
bool satnogs_radio::valid_mode_string()
{
	return (!mode_string.empty()) && (demodulator_registry.find(mode_string) != demodulator_registry.end());
}

/** \brief checks if decoder string is valid - that means not empty and the decoder is avaiblable via plugin */
bool satnogs_radio::valid_decoder_string()
{
	return (!decoder_string.empty()) && (decoder_registry.find(decoder_string) != decoder_registry.end());
}

/** \brief checks if source string is valid - that means not empty and the source is avaiblable supported */
bool satnogs_radio::valid_source_string()
{
	return (!source_string.empty()) && 
	       (std::find(valid_sources.begin(), valid_sources.end(), source_string) != valid_sources.end());
}

/** \brief checks if source filename string is valid - that means not empty and the exists */
bool satnogs_radio::valid_source_filename_string()
{
	return (!source_filename.empty()) && boost::filesystem::exists(source_filename);
}

// C++-C-Bridge

extern "C" {
#include "gnuradio_c_wrapper.h"
}

static satnogs_radio* my_satnogs_radio;

void init_gnuradio() { my_satnogs_radio = new satnogs_radio(); }

void start_gnuradio() { my_satnogs_radio->start_radio(); }

void stop_gnuradio() { my_satnogs_radio->stop_radio(); }

void satnogs_gr_set_args(const char* args) { my_satnogs_radio->set_arg(args); }

void satnogs_gr_set_mode(const char* mode) { my_satnogs_radio->set_mode(mode); }

void satnogs_gr_set_decoder(const char* decoder)
{
	my_satnogs_radio->set_decoder(decoder);
}

void satnogs_gr_set_source(const char* source) { my_satnogs_radio->set_source(source); }

void satnogs_gr_set_freq(double freq) { my_satnogs_radio->set_center_freq(freq); }

void satnogs_gr_set_gain(double gain) { my_satnogs_radio->set_gain(gain); }

void satnogs_gr_set_if_gain(double gain) { my_satnogs_radio->set_if_gain(gain); }

void satnogs_gr_set_bb_gain(double gain) { my_satnogs_radio->set_bb_gain(gain); }

void satnogs_gr_set_ogg_filename(char* filename)
{
	my_satnogs_radio->set_ogg_filename(filename);
}

void satnogs_gr_set_source_filename(char* filename)
{
	my_satnogs_radio->set_source_filename(filename);
}

void satnogs_gr_set_rx_rate(double rate) { my_satnogs_radio->set_rx_rate(rate); }

void satnogs_gr_set_lo(double lo_offset) { my_satnogs_radio->set_lo(lo_offset); }

void satnogs_gr_set_frame_file_prefix(const char* frame_file_prefix) {
	my_satnogs_radio->set_frame_file_prefix(frame_file_prefix);
}

void satnogs_gr_set_waterfall_file(const char* waterfall) {
	my_satnogs_radio->set_waterfall_file(waterfall);
}

void satnogs_gr_set_exit(int i) {
	my_satnogs_radio->set_exit(i);
}

void satnogs_gr_set_audio_out(int i) {
	my_satnogs_radio->set_audio_out(i);
}
