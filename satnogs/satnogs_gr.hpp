/** \file
 * \brief Header for the GNU Radio part of SatNOGS-Radio */
#include "plugins/plugin.hpp"
#include <gnuradio/analog/noise_source_c.h>
#include <gnuradio/audio/sink.h>
#include <gnuradio/blocks/file_source.h>
#include <gnuradio/blocks/udp_sink.h>
#include <gnuradio/blocks/null_sink.h>
#include <gnuradio/blocks/wavfile_source.h>
#include <gnuradio/top_block.h>
#include <osmosdr/source.h>
#include <satnogs/ogg_encoder.h>
#include <satnogs/frame_file_sink.h>
#include <satnogs/waterfall_sink.h>
#include <boost/dll/import.hpp>

using namespace gr;

/// \brief Main Class for GNU Radio related SatNOGS-Radio
class satnogs_radio
{
private:
	hier_block2_sptr demodulator_block;
	hier_block2_sptr decoder_block;
	osmosdr::source::sptr sdr;
	audio::sink::sptr audio_out;
	satnogs::ogg_encoder::sptr ogg_encoder;
	gr::blocks::wavfile_source::sptr wavfile_source;
	gr::blocks::file_source::sptr iqfile_source;
	gr::blocks::udp_sink::sptr udp_sink;
	gr::blocks::null_sink::sptr null_float_sink;
	gr::satnogs::frame_file_sink::sptr frame_file_sink;
	gr::satnogs::waterfall_sink::sptr waterfall_sink;
	std::map<std::string, boost::shared_ptr<satnogs_demodulator_block>>
	    demodulator_registry;
	std::map<std::string, boost::shared_ptr<satnogs_decoder_block>> decoder_registry;
	boost::shared_ptr<satnogs_demodulator_block> demodulator;
	boost::shared_ptr<satnogs_decoder_block> decoder;
	// Variables:
	int audio_samp_rate = 48'000;
	double rx_sampling_rate = 2'000'000;
	double lo_offset = 250'000;
	double f_rx = 145'000'000;
	bool running = false;
	std::string args;
	gr::basic_block_sptr source;
	std::string mode_string = "";
	std::string decoder_string = "";
	std::string source_string = "";
	std::string source_filename = "";
	std::string frame_file_prefix = "";
	std::string ogg_output_file = "";
	std::string waterfall_file_path = "";
	std::vector<std::string> valid_sources {"SDR", "WavFile", "IQFile", "OggFile"};
	bool exit_after_finish = false;
	bool is_float_source = false;
	bool use_audio_out = false;
	bool instantiate_demodulator();
	bool instantiate_decoder();
	bool instantiate_source();
	void instantiate_waterfall();
	bool valid_mode_string();
	bool valid_decoder_string();
	bool valid_source_string();
	bool valid_source_filename_string();
	void connect_demodulator_outputs();
	void connect_audio_out();
	void update_float_source_state(bool new_state);
	void instantiate_ogg_output();
	void stop_and_exit();
	top_block_sptr tb;

public:
	satnogs_radio();
	~satnogs_radio();

	void start_radio();
	void stop_radio();
	int get_samp_rate() const;
	void set_samp_rate(int samp_rate);
	void set_rx_rate(double rate);
	void set_mode(std::string mode);
	void set_decoder(std::string decoder);
	void set_center_freq(double freq);
	void set_gain(double gain);
	void set_if_gain(double gain);
	void set_bb_gain(double gain);
	void set_ogg_filename(const std::string &filename);
	void set_source_filename(char* filename);
	void set_source(const std::string& source);
	void set_lo(double lo_offset);
	void set_arg(const std::string& args);
	void set_frame_file_prefix(const std::string &prefix);
	void set_waterfall_file(const std::string &waterfall);
	void set_exit(int i);
	void set_audio_out(int i);
};
