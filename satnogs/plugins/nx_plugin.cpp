#include "plugin.hpp"
#include <gnuradio/blocks/message_debug.h>
#include <gnuradio/hier_block2.h>
#include <tnc_nx/nx_decoder.h>
#include <boost/config.hpp>
#include <pmt/pmt.h>

#ifdef gnuradio_nx_hier_block_EXPORTS
#define HIER_BLOCK_TEST_API __GR_ATTR_EXPORT
#else
#define HIER_BLOCK_TEST_API __GR_ATTR_IMPORT
#endif

namespace nx_plugin {

class HIER_BLOCK_TEST_API nx_hier_block : virtual public gr::hier_block2
{
public:
	typedef boost::shared_ptr<nx_hier_block> sptr;

	/*!
	 * \brief Return a shared_ptr to a new instance of hier_block_test::hier_test.
	 *
	 * To avoid accidental use of raw pointers, hier_block_test::hier_test's
	 * constructor is in a private implementation
	 * class. hier_block_test::hier_test::make is the public interface for
	 * creating new instances.
	 */
	static sptr make();
};

class nx_hier_block_impl : public nx_hier_block
{
private:
	gr::tnc_nx::nx_decoder::sptr nx_decoder;
	gr::blocks::message_debug::sptr msg_debug;

public:
	nx_hier_block_impl();
	~nx_hier_block_impl();
};

nx_hier_block::sptr nx_hier_block::make()
{
	return gnuradio::get_initial_sptr(new nx_hier_block_impl());
}

nx_hier_block_impl::nx_hier_block_impl()
    : gr::hier_block2("nx_hier_block",
                      gr::io_signature::make(1, 1, sizeof(int8_t)),
                      gr::io_signature::make(0, 0, 0))
{
	this->nx_decoder = gr::tnc_nx::nx_decoder::make(0x0EF0, true);
	this->msg_debug = gr::blocks::message_debug::make();
	this->message_port_register_hier_out (pmt::mp("out"));

	this->connect(self(), 0, this->nx_decoder, 0);
	this->msg_connect(this->nx_decoder, "out", this->msg_debug, "print_pdu");
	this->msg_connect(this->nx_decoder, "out", self(), "out");
}

nx_hier_block_impl::~nx_hier_block_impl() {}

class satnogs_nx_demod : public satnogs_decoder_block
{
private:
	gr::hier_block2_sptr nx_decoder;

public:
	satnogs_nx_demod() { rig_debug(RIG_DEBUG_VERBOSE, "Constructing nx plugin\n"); }

	std::string name() const { return "NX"; }

	~satnogs_nx_demod() { rig_debug(RIG_DEBUG_VERBOSE, "Destructing nx plugin\n"); }

	gr::hier_block2_sptr block()
	{
		this->nx_decoder = nx_hier_block::make();
		return this->nx_decoder;
	}
};

extern "C" BOOST_SYMBOL_EXPORT satnogs_nx_demod plugin;
satnogs_nx_demod plugin;

} // namespace nx_plugin
