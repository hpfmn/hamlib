#include "plugin.hpp"
#include <gnuradio/analog/quadrature_demod_cf.h>
#include <gnuradio/attributes.h>
#include <gnuradio/blocks/multiply_const_ff.h>
#include <gnuradio/digital/binary_slicer_fb.h>
#include <gnuradio/digital/clock_recovery_mm_ff.h>
#include <gnuradio/filter/dc_blocker_ff.h>
#include <gnuradio/filter/fir_filter_fff.h>
#include <gnuradio/filter/firdes.h>
#include <gnuradio/filter/freq_xlating_fir_filter_ccf.h>
#include <gnuradio/filter/rational_resampler_base_ccf.h>
#include <gnuradio/hier_block2.h>
#include <gnuradio/io_signature.h>
#include <boost/config.hpp>
#include <iostream>

#ifdef gnuradio_gmsk_hier_block_EXPORTS
#define HIER_BLOCK_TEST_API __GR_ATTR_EXPORT
#else
#define HIER_BLOCK_TEST_API __GR_ATTR_IMPORT
#endif

namespace gmsk_plugin {

int greatest_common_divisor(int a, int b)
{
	int x;
	while (b > 0) {
		x = a % b;
		a = b;
		b = x;
	}
	return a;
}

class HIER_BLOCK_TEST_API gmsk_hier_block : virtual public gr::hier_block2
{
public:
	typedef boost::shared_ptr<gmsk_hier_block> sptr;

	/*!
	 * \brief Return a shared_ptr to a new instance of hier_block_test::hier_test.
	 *
	 * To avoid accidental use of raw pointers, hier_block_test::hier_test's
	 * constructor is in a private implementation
	 * class. hier_block_test::hier_test::make is the public interface for
	 * creating new instances.
	 */
	static sptr make(int audio_sampling_rate,
	                 double rx_sampling_rate,
	                 double lo_offset,
	                 bool audio_input);
	virtual void set_center_freq(double center_freq) = 0;
};


class gmsk_hier_block_impl : public gmsk_hier_block
{
private:
	int audio_sampling_rate;
	double rx_sampling_rate;
	double lo_offset;
	double filter_rate = 500'000;
	const float PI = 3.145926;
	float max_dev = 75e3;
	int audio_decimate = 10;
	gr::filter::freq_xlating_fir_filter_ccf::sptr fir_filter;
	gr::analog::quadrature_demod_cf::sptr quad_demod;
	gr::filter::fir_filter_fff::sptr audio_filter;
	gr::filter::rational_resampler_base_ccf::sptr resampler;
	gr::filter::dc_blocker_ff::sptr dc_blocker;
	gr::digital::clock_recovery_mm_ff::sptr mm_clock_recovery;
	gr::digital::binary_slicer_fb::sptr binary_slicer;
	gr::blocks::multiply_const_ff::sptr multiply_const;
	void init_fir_filter();
	void init_quad_demod();
	void init_audio_filter();
	void init_resampler();
	void init_dc_blocker();
	void init_multiply_const();
	void init_clock_recovery_mm();
	void init_binary_slicer_fb();
	void set_center_freq(double center_freq);

public:
	gmsk_hier_block_impl(int audio_sampling_rate,
	                     double rx_sampling_rate,
	                     double lo_offset,
	                     bool audio_input);
	~gmsk_hier_block_impl();
};

gmsk_hier_block::sptr gmsk_hier_block::make(int audio_sampling_rate,
                                            double rx_sampling_rate,
                                            double lo_offset,
                                            bool audio_input)
{
	return gnuradio::get_initial_sptr(new gmsk_hier_block_impl(
	    audio_sampling_rate, rx_sampling_rate, lo_offset, audio_input));
}

gmsk_hier_block_impl::gmsk_hier_block_impl(int audio_sampling_rate,
                                           double rx_sampling_rate,
                                           double lo_offset,
                                           bool audio_input)
    : gr::hier_block2(
          "gmsk_hier_block",
          gr::io_signature::make(1, 1, audio_input ? sizeof(float) : sizeof(gr_complex)),
          audio_input ?
          gr::io_signature::make2(2, 2, sizeof(float), sizeof(int8_t)) :
          gr::io_signature::make3(3, 3, sizeof(float), sizeof(int8_t), sizeof(gr_complex))),
      audio_sampling_rate(audio_sampling_rate),
      rx_sampling_rate(rx_sampling_rate),
      lo_offset(lo_offset)
{
	init_dc_blocker();
	init_multiply_const();
	init_clock_recovery_mm();
	init_binary_slicer_fb();

	if (!audio_input) {
		init_fir_filter();
		init_quad_demod();
		init_audio_filter();
		init_resampler();

		connect(self(), 0, fir_filter, 0);
		connect(fir_filter, 0, resampler, 0);
		connect(resampler, 0, quad_demod, 0);
		connect(quad_demod, 0, audio_filter, 0);
		connect(audio_filter, 0, self(), 0);
		connect(resampler, 0, self(), 2);

		connect(audio_filter, 0, dc_blocker, 0);
	} else {
		gr::blocks::multiply_const_ff::sptr multiply_const2 =
		    gr::blocks::multiply_const_ff::make(1.0);
		connect(self(), 0, multiply_const2, 0);
		connect(multiply_const2, 0, self(), 0);
		connect(self(), 0, dc_blocker, 0);
	}

	connect(dc_blocker, 0, multiply_const, 0);
	connect(multiply_const, 0, mm_clock_recovery, 0);
	connect(mm_clock_recovery, 0, binary_slicer, 0);
	connect(binary_slicer, 0, self(), 1);
}

void gmsk_hier_block_impl::init_fir_filter()
{
	int fir_decimation = rx_sampling_rate / filter_rate;
	std::vector<float> taps =
	    gr::filter::firdes::low_pass(1, rx_sampling_rate, 100'000, 100'000);

	fir_filter = gr::filter::freq_xlating_fir_filter_ccf::make(
	    fir_decimation, taps, lo_offset, rx_sampling_rate);
}

void gmsk_hier_block_impl::init_quad_demod()
{
	// TODO: Check gain value
	quad_demod = gr::analog::quadrature_demod_cf::make(0.5);
}

void gmsk_hier_block_impl::init_audio_filter()
{
	double width_of_transition_band = audio_sampling_rate / 32;
	std::vector<float> audio_taps =
	    gr::filter::firdes::low_pass(1.0,
	                                 filter_rate,
	                                 audio_sampling_rate / 2 - width_of_transition_band,
	                                 width_of_transition_band);

	audio_filter = gr::filter::fir_filter_fff::make(audio_decimate, audio_taps);
}

void gmsk_hier_block_impl::set_center_freq(double center_freq)
{
	this->fir_filter->set_center_freq(center_freq);
}

void gmsk_hier_block_impl::init_resampler()
{
	int gcd = greatest_common_divisor(audio_sampling_rate, filter_rate / audio_decimate);
	int interpolation = audio_sampling_rate / gcd;
	int decimation = (filter_rate / audio_decimate) / gcd;
	double rate = (float)interpolation / (float)decimation;
	double fractional_bw = 0.4;
	double beta = 7.0;
	double halfband = 0.5;

	double trans_width = rate * (halfband - fractional_bw);
	double mid_transition_band = rate * halfband - trans_width / 2.0f;

	std::vector<float> rr_taps =
	    gr::filter::firdes::low_pass(interpolation,
	                                 interpolation,
	                                 mid_transition_band,
	                                 trans_width,
	                                 gr::filter::firdes::WIN_KAISER,
	                                 beta);

	resampler =
	    gr::filter::rational_resampler_base_ccf::make(interpolation, decimation, rr_taps);
}

void gmsk_hier_block_impl::init_dc_blocker()
{
	dc_blocker = gr::filter::dc_blocker_ff::make(1000, false);
}

void gmsk_hier_block_impl::init_clock_recovery_mm()
{
	float samp_per_sym = 10.0;
	mm_clock_recovery = gr::digital::clock_recovery_mm_ff::make(
	    samp_per_sym, 0.25 * 0.175 * 0.175, 0.5, 0.175, 0.005);
}

void gmsk_hier_block_impl::init_binary_slicer_fb()
{
	binary_slicer = gr::digital::binary_slicer_fb::make();
}

void gmsk_hier_block_impl::init_multiply_const()
{
	// TODO: inversion
	multiply_const = gr::blocks::multiply_const_ff::make(-1.0);
}

/*
 * Our virtual destructor.
 */
gmsk_hier_block_impl::~gmsk_hier_block_impl() {}


class satnogs_gmsk_demod : public satnogs_demodulator_block
{
private:
	gmsk_hier_block::sptr gmsk_decoder;

public:
	satnogs_gmsk_demod() { rig_debug(RIG_DEBUG_VERBOSE, "Constructing gmsk plugin\n"); }

	std::string name() const { return "GMSK"; }

	~satnogs_gmsk_demod() { rig_debug(RIG_DEBUG_VERBOSE, "Destructing gmsk plugin\n"); }

	gr::hier_block2_sptr block(int audio_sampling_rate,
	                           double rx_sampling_rate,
	                           double lo_offset,
	                           bool audio_input)
	{
		this->gmsk_decoder = gmsk_hier_block::make(
		    audio_sampling_rate, rx_sampling_rate, lo_offset, audio_input);
		return this->gmsk_decoder;
	}

	bool has_audio_out() { return true; }

	bool has_binary_out() { return true; }

	bool has_complex_out() { return true; }

	void set_center_freq(double center_freq)
	{
		this->gmsk_decoder->set_center_freq(center_freq);
	}
};

extern "C" BOOST_SYMBOL_EXPORT satnogs_gmsk_demod plugin;
satnogs_gmsk_demod plugin;

} // namespace gmsk_plugin
