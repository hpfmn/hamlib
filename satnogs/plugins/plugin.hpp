/** \file
 * \brief Header file for the SatNOGS-Radio plugin API */
#include "../../include/hamlib/rig.h"
#include <gnuradio/hier_block2.h>
#include <string>

/** \brief Plugin-Type (demodualtor or decoder)*/
enum class satnogs_plugin_type { SATNOGS_DEMODULATOR, SATNOGS_DECODER };

/** \brief generic SatNOGS-Radio Plugin Class - very minimalistic API*/
class satnogs_plugin_block
{
public:
	virtual std::string name() const = 0;
	/// \brief return plugin type
	virtual satnogs_plugin_type get_plugin_type() = 0;
	virtual ~satnogs_plugin_block() {}
};

/** \brief Demodulator SatNOGS-Radio Plugin Class */
class satnogs_demodulator_block : public satnogs_plugin_block
{
public:
	satnogs_plugin_type get_plugin_type()
	{
		return satnogs_plugin_type::SATNOGS_DEMODULATOR;
	};

	/** \brief returns a demodulator block - it is expected that calling this method
	* multiple times will return a new block each time */
	virtual gr::hier_block2_sptr block(int audio_sampling_rate = 48'000,
	                                   double rx_sampling_rate = 2'000'000,
	                                   double lo_offset = 250'000,
	                                   bool audio_input = false) = 0;

	/// \brief pure virtual funciton to set the center frequency (aka LO-Offset)
	virtual void set_center_freq(double center_freq) = 0;

	virtual bool has_audio_out() { return false; };
	virtual bool has_binary_out() { return false; };
	virtual bool has_complex_out() { return false; };
};

/** \brief Docoder SatNOGS-Radio Plugin Class */
class satnogs_decoder_block : public satnogs_plugin_block
{
public:
	satnogs_plugin_type get_plugin_type()
	{
		return satnogs_plugin_type::SATNOGS_DECODER;
	};
	virtual gr::hier_block2_sptr block() = 0;
};
