#include "plugin.hpp"
#include <gnuradio/analog/quadrature_demod_cf.h>
#include <gnuradio/attributes.h>
#include <gnuradio/filter/fir_filter_fff.h>
#include <gnuradio/filter/firdes.h>
#include <gnuradio/filter/freq_xlating_fir_filter_ccf.h>
#include <gnuradio/filter/rational_resampler_base_fff.h>
#include <gnuradio/hier_block2.h>
#include <gnuradio/io_signature.h>
#include <boost/config.hpp>
#include <iostream>

#ifdef gnuradio_wfm_hier_block_EXPORTS
#define HIER_BLOCK_TEST_API __GR_ATTR_EXPORT
#else
#define HIER_BLOCK_TEST_API __GR_ATTR_IMPORT
#endif

namespace wfm_plugin {

int greatest_common_divisor(int a, int b)
{
	int x;
	while (b > 0) {
		x = a % b;
		a = b;
		b = x;
	}
	return a;
}

class HIER_BLOCK_TEST_API wfm_hier_block : virtual public gr::hier_block2
{
public:
	typedef boost::shared_ptr<wfm_hier_block> sptr;

	/*!
	 * \brief Return a shared_ptr to a new instance of hier_block_test::hier_test.
	 *
	 * To avoid accidental use of raw pointers, hier_block_test::hier_test's
	 * constructor is in a private implementation
	 * class. hier_block_test::hier_test::make is the public interface for
	 * creating new instances.
	 */
	static sptr make(int audio_sampling_rate, double rx_sampling_rate, double lo_offset);
	void set_center_freq(double center_freq);
};


class wfm_hier_block_impl : public wfm_hier_block
{
private:
	int audio_sampling_rate;
	double rx_sampling_rate;
	double lo_offset;
	double filter_rate = 500'000;
	const float PI = 3.145926;
	float max_dev = 75e3;
	int audio_decimate = 10;
	gr::filter::freq_xlating_fir_filter_ccf::sptr fir_filter;
	gr::analog::quadrature_demod_cf::sptr quad_mod;
	gr::filter::fir_filter_fff::sptr audio_filter;
	gr::filter::rational_resampler_base_fff::sptr resampler;
	void init_fir_filter();
	void init_quad_mod();
	void init_audio_filter();
	void init_resampler();
	void set_center_freq(double center_freq);

public:
	wfm_hier_block_impl(int audio_sampling_rate,
	                    double rx_sampling_rate,
	                    double lo_offset);
	~wfm_hier_block_impl();
};

wfm_hier_block::sptr
wfm_hier_block::make(int audio_sampling_rate, double rx_sampling_rate, double lo_offset)
{
	return gnuradio::get_initial_sptr(
	    new wfm_hier_block_impl(audio_sampling_rate, rx_sampling_rate, lo_offset));
}

wfm_hier_block_impl::wfm_hier_block_impl(int audio_sampling_rate,
                                         double rx_sampling_rate,
                                         double lo_offset)
    : gr::hier_block2("wfm_hier_block",
                      gr::io_signature::make(1, 1, sizeof(gr_complex)),
                      gr::io_signature::make(1, 1, sizeof(float))),
      audio_sampling_rate(audio_sampling_rate),
      rx_sampling_rate(rx_sampling_rate),
      lo_offset(lo_offset)
{
	init_fir_filter();
	init_quad_mod();
	init_audio_filter();
	init_resampler();

	connect(self(), 0, fir_filter, 0);
	connect(fir_filter, 0, quad_mod, 0);
	connect(quad_mod, 0, audio_filter, 0);
	connect(audio_filter, 0, resampler, 0);
	connect(resampler, 0, self(), 0);
}

void wfm_hier_block_impl::set_center_freq(double center_freq)
{
	this->fir_filter->set_center_freq(center_freq);
}

void wfm_hier_block_impl::init_fir_filter()
{
	int fir_decimation = rx_sampling_rate / filter_rate;
	std::vector<float> taps =
	    gr::filter::firdes::low_pass(1, rx_sampling_rate, 100'000, 100'000);

	fir_filter = gr::filter::freq_xlating_fir_filter_ccf::make(
	    fir_decimation, taps, lo_offset, rx_sampling_rate);
}

void wfm_hier_block_impl::init_quad_mod()
{
	quad_mod = gr::analog::quadrature_demod_cf::make(filter_rate / (2 * PI * max_dev));
}

void wfm_hier_block_impl::init_audio_filter()
{
	double width_of_transition_band = audio_sampling_rate / 32;
	std::vector<float> audio_taps =
	    gr::filter::firdes::low_pass(1.0,
	                                 filter_rate,
	                                 audio_sampling_rate / 2 - width_of_transition_band,
	                                 width_of_transition_band);

	audio_filter = gr::filter::fir_filter_fff::make(audio_decimate, audio_taps);
}

void wfm_hier_block_impl::init_resampler()
{
	int gcd = greatest_common_divisor(audio_sampling_rate, filter_rate / audio_decimate);
	int interpolation = audio_sampling_rate / gcd;
	int decimation = (filter_rate / audio_decimate) / gcd;
	double rate = (float)interpolation / (float)decimation;
	double fractional_bw = 0.4;
	double beta = 7.0;
	double halfband = 0.5;

	double trans_width = rate * (halfband - fractional_bw);
	double mid_transition_band = rate * halfband - trans_width / 2.0f;

	std::vector<float> rr_taps =
	    gr::filter::firdes::low_pass(interpolation,
	                                 interpolation,
	                                 mid_transition_band,
	                                 trans_width,
	                                 gr::filter::firdes::WIN_KAISER,
	                                 beta);

	resampler =
	    gr::filter::rational_resampler_base_fff::make(interpolation, decimation, rr_taps);
}

/*
 * Our virtual destructor.
 */
wfm_hier_block_impl::~wfm_hier_block_impl() {}


class satnogs_wfm_demod : public satnogs_demodulator_block
{
private:
	wfm_hier_block::sptr wfm_decoder;

public:
	satnogs_wfm_demod() { rig_debug(RIG_DEBUG_VERBOSE, "Destructing wfm plugin\n"); }

	std::string name() const { return "WFM"; }

	~satnogs_wfm_demod() { rig_debug(RIG_DEBUG_VERBOSE, "Destructing wfm plugin\n"); }

	gr::hier_block2_sptr block(int audio_sampling_rate,
	                           double rx_sampling_rate,
	                           double lo_offset,
	                           bool audio_input)
	{
		this->wfm_decoder =
		    wfm_hier_block::make(audio_sampling_rate, rx_sampling_rate, lo_offset);
		return this->wfm_decoder;
	}

	bool has_audio_out() { return true; }

	void set_center_freq(double center_freq)
	{
		this->wfm_decoder->set_center_freq(center_freq);
	}
};

extern "C" BOOST_SYMBOL_EXPORT satnogs_wfm_demod plugin;
satnogs_wfm_demod plugin;

} // namespace wfm_plugin
