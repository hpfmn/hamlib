/*
 *  Hamlib Dummy backend - main header
 *  Copyright (c) 2001-2009 by Stephane Fillod
 *
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef _SATNOGS_H
#define _SATNOGS_H 1

#include "hamlib/rig.h"
#include "token.h"

/* backend conf */
#define TOK_CFG_MAGICCONF TOKEN_BACKEND(1)
#define TOK_CFG_STATIC_DATA TOKEN_BACKEND(2)
#define TOK_CFG_ARG TOKEN_BACKEND(3)
#define TOK_CFG_OGG TOKEN_BACKEND(4)
#define TOK_CFG_MODE TOKEN_BACKEND(5)
#define TOK_CFG_DEC TOKEN_BACKEND(6)
#define TOK_CFG_SRC TOKEN_BACKEND(7)
#define TOK_CFG_RXR TOKEN_BACKEND(8)
#define TOK_CFG_LO TOKEN_BACKEND(9)
#define TOK_CFG_FRPRE TOKEN_BACKEND(10)


/* ext_level's and ext_parm's tokens */
#define TOK_EP_MODE TOKEN_BACKEND(1)
#define TOK_EP_DEC TOKEN_BACKEND(2)
#define TOK_EP_SRC TOKEN_BACKEND(3)
#define TOK_EL_GAIN TOKEN_BACKEND(4)
#define TOK_EL_BBGAIN TOKEN_BACKEND(5)
#define TOK_EL_IFGAIN TOKEN_BACKEND(6)
#define TOK_EP_MAGICPARM TOKEN_BACKEND(7)
#define TOK_EP_ARG TOKEN_BACKEND(8)
#define TOK_EP_OGG TOKEN_BACKEND(9)
#define TOK_EP_SRCFILE TOKEN_BACKEND(10)
#define TOK_EL_RXR TOKEN_BACKEND(11)
#define TOK_EL_LO TOKEN_BACKEND(12)
#define TOK_EP_FRPRE TOKEN_BACKEND(13)
#define TOK_EP_WATERFALL TOKEN_BACKEND(14)
#define TOK_EP_EXIT TOKEN_BACKEND(15)
#define TOK_EP_AUDIO_OUT TOKEN_BACKEND(16)

extern const struct rig_caps satnogs_caps;

#endif /* _SATNOGS_H */
